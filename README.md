# Timesheet logger

A CLI-based timesheet logger for university projects.

## Installation

```$ npm install -g timesheet-logger```

## Usage

``` $ tlog <project> [mins] [description]```

### Example

If we worked on writing a backend endpoint to fetch a user from the datastore for 40 minutes, we would use:

```$ tlog "software-engineering" 40 "Writing backend endpoint for users"```

#### Response

```
Saved new record: 40, Writing backend endpoint for users
```

Let's say now we want to examine the contents of our log file. We retrieve this data by giving the tool the name of the project we want to examine the logfile from.

```$ tlog software-engineering```

#### Response

```
┌─────────┬──────────────────────┬───────────────────────────────────┐
│ (index) │ Time spent (minutes) │            description            │
├─────────┼──────────────────────┼───────────────────────────────────┤
│    0    │         '40'         │ 'Wrote backend enpoint for users' │
└─────────┴──────────────────────┴───────────────────────────────────┘
```

### Storage

Your log files will be stored in the following folder:

```/home/$USER/tlog```