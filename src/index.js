#!/usr/bin/node

const fs = require('fs');
const readline = require('readline');
const csvtojson = require('csvtojson/v2');

(async () => {
    
    const logsFolder = `/home/${process.env.USER}/tlog`;
    
    if (!fs.existsSync(logsFolder)) {
        fs.mkdirSync(logsFolder);
    }

    if (process.argv.length <= 2) {
        console.log("Usage: $ tlog <course> [mins] [description], for more info see the readme");
        process.exit(0);
    } else if (process.argv.length == 3) {

        /**
         * Print contents of the requested logfile
         */
        const [ projectname ] = process.argv.slice(2);
        var logFile = `${logsFolder}/${projectname}.log`;

        if (fs.existsSync(logFile)) {
            csvtojson().fromFile(logFile).then((jsonObj) => {
                console.table(jsonObj);
            });
        } else {
            console.log(`Project "${projectname}" does not exist`);
        }

    } else if (process.argv.length == 5) {

        /**
         * Add record to the given logfile
         */

        const [ projectname, minutes, description ] = process.argv.slice(2);
        
        var currentDate = new Date().toString();

        var newRecord = `${minutes}, ${description}, ${currentDate}`;
        var logFile = `${logsFolder}/${projectname}.log`;

        if (fs.existsSync(logFile)) {

            /**
             * If file exists, append the new record
             */

            await appendToFile(logFile, newRecord);
            console.log(`Saved new record: "${newRecord}"`);
        } else {
            
            /**
             * If file doesn't exist, prompt user on whether the file should be created
             */

            var rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });

            rl.question('No logfile for this course exists, create it? (y/n) ', (answer) => {
                if (answer == 'y') {
                    appendToFile(logFile, "Time spent (minutes), Description, Date");
                    appendToFile(logFile, newRecord);
                    console.log(`Saved new record: "${newRecord}"`);
                }
                rl.close();
            });
        }
    }
})().catch((err) => {
    console.log(err);
});

/**
 * Append a record to a given file
 * @param {string} file Filepath to the logfile 
 * @param {string} record Record that must be added to given logfile
 */
async function appendToFile(file, record) {
    await fs.appendFile(file, `${record}\n`, (err) => {
        if (err) throw err;
    });
}